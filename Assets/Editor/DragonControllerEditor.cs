﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DragonController))]
public class DragonControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DragonController dragonController = (DragonController)target;
        
        bool allowSceneObjects = !EditorUtility.IsPersistent(target);
        dragonController.DragonAttackType = (DragonAttackType)EditorGUILayout.EnumPopup("DragonAttackType", dragonController.DragonAttackType);
        if (dragonController.DragonAttackType == DragonAttackType.BiteAttack)
        {
            //Appointed object
            dragonController.Animator = (Animator)EditorGUILayout.ObjectField("Animator", dragonController.Animator, typeof(Animator), allowSceneObjects);
            dragonController.RigidBody2D = (Rigidbody2D)EditorGUILayout.ObjectField("RigidBody2D", dragonController.RigidBody2D, typeof(Rigidbody2D), allowSceneObjects);
            dragonController.AttackPoint = (Transform)EditorGUILayout.ObjectField("AttackPoint", dragonController.AttackPoint, typeof(Transform), allowSceneObjects);
            dragonController.Knight = (GameObject)EditorGUILayout.ObjectField("Knight", dragonController.Knight, typeof(GameObject), allowSceneObjects);
            //KnightProperties
            dragonController.Health = EditorGUILayout.FloatField("Health", dragonController.Health);
            dragonController.Speed = EditorGUILayout.Slider("Speed", dragonController.Speed, 0, 5);
            //AttackProperties
            dragonController.AttackRadius = EditorGUILayout.Slider("Attack_1_Radius", dragonController.AttackRadius, 0, 0.6f);
            dragonController.Damage = EditorGUILayout.FloatField("Attack_1_Damage", dragonController.Damage);
        }
        else if (dragonController.DragonAttackType == DragonAttackType.FireBallAttack)
        {
            //Appointed object
            dragonController.Animator = (Animator)EditorGUILayout.ObjectField("Animator", dragonController.Animator, typeof(Animator), allowSceneObjects);
            dragonController.RigidBody2D = (Rigidbody2D)EditorGUILayout.ObjectField("RigidBody2D", dragonController.RigidBody2D, typeof(Rigidbody2D), allowSceneObjects);
            dragonController.FireBallRespawn = (Transform)EditorGUILayout.ObjectField("FireBallRespawn", dragonController.FireBallRespawn, typeof(Transform), allowSceneObjects);
            dragonController.Knight = (GameObject)EditorGUILayout.ObjectField("Knight", dragonController.Knight, typeof(GameObject), allowSceneObjects);
            dragonController.Sight = (Transform)EditorGUILayout.ObjectField("Sight", dragonController.Sight, typeof(Transform), allowSceneObjects);
            //KnightProperties
            dragonController.Health = EditorGUILayout.FloatField("Health", dragonController.Health);
            dragonController.Speed = EditorGUILayout.Slider("Speed", dragonController.Speed, 0, 5);
            //AttackProperties
            dragonController.FireBallDamage = EditorGUILayout.FloatField("FireBallDamage", dragonController.FireBallDamage);
            dragonController.FireBallSpeed = EditorGUILayout.Slider("FireBallSpeed", dragonController.FireBallSpeed, 0, 5);
            dragonController.KnightOnSight1 = EditorGUILayout.Toggle("Sight", dragonController.KnightOnSight1);
        }
    }
}
