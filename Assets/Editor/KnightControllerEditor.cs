﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(KnightController))]
public class KnightControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        KnightController knightController = (KnightController)target;
        //Appointed object
        bool allowSceneObjects = !EditorUtility.IsPersistent(target);
        knightController.Animator = (Animator)EditorGUILayout.ObjectField("Animator", knightController.Animator, typeof(Animator), allowSceneObjects);
        knightController.RigidBody2D = (Rigidbody2D)EditorGUILayout.ObjectField("RigidBody2D", knightController.RigidBody2D,typeof(Rigidbody2D),allowSceneObjects);
        knightController.AttackPoint = (Transform)EditorGUILayout.ObjectField("AttackPoint", knightController.AttackPoint, typeof(Transform), allowSceneObjects);
        knightController.FireBallRespawn = (Transform)EditorGUILayout.ObjectField("FireBallRespawn", knightController.FireBallRespawn, typeof(Transform), allowSceneObjects);
        knightController.MainCamera = (Camera)EditorGUILayout.ObjectField("MainCamera", knightController.MainCamera, typeof(Camera), allowSceneObjects);
        knightController.GroundCheck = (Transform)EditorGUILayout.ObjectField("GroundCheck", knightController.GroundCheck, typeof(Transform), allowSceneObjects);
        knightController.KnightWalk = (KnightWalk)EditorGUILayout.ObjectField("KnightWalk", knightController.KnightWalk, typeof(KnightWalk), allowSceneObjects);
        knightController.JumpSmokeRespawn = (Transform)EditorGUILayout.ObjectField("JumpSmokeRespawn", knightController.JumpSmokeRespawn, typeof(Transform), allowSceneObjects);
        //KnightProperties
        knightController.Health = EditorGUILayout.FloatField("Health", knightController.Health);
        knightController.Mana = EditorGUILayout.FloatField("Mana", knightController.Mana);
        knightController.Speed = EditorGUILayout.Slider("Speed", knightController.Speed, 0, 5);
        knightController.SpeedOnStair = EditorGUILayout.Slider("SpeedOnStair", knightController.SpeedOnStair, 0, 5);
        knightController.JumpForce = EditorGUILayout.FloatField("JumpForce", knightController.JumpForce);
        knightController.SecondJump = EditorGUILayout.Toggle("SecondJump", knightController.SecondJump);
        //AttackProperties
        knightController.AttackRadius = EditorGUILayout.Slider("Attack_1_Radius", knightController.AttackRadius, 0, 0.6f);
        knightController.Damage = EditorGUILayout.FloatField("Attack_1_Damage", knightController.Damage);
        knightController.FireBallDamage = EditorGUILayout.FloatField("Attack_2_Damage", knightController.FireBallDamage);
        knightController.FireBallSpeed = EditorGUILayout.Slider("Attack_2_Speed", knightController.FireBallSpeed, 0, 5);
        knightController.SecondAttackCost = EditorGUILayout.FloatField("Attack_2_ManaCost", knightController.SecondAttackCost);


    }
}


