﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FireBall))]
public class FireBallEditor : Editor
{
    public override void OnInspectorGUI()
    {
        FireBall _fireBall = (FireBall)target;

        bool allowSceneObjects = !EditorUtility.IsPersistent(target);

        _fireBall.FireballOwner = (FireBallOwner)EditorGUILayout.EnumPopup("FireballOwner", _fireBall.FireballOwner);
        //Appointed object
        if (_fireBall.FireballOwner == FireBallOwner.Knight)
        {
            _fireBall.KnightController = (KnightController)EditorGUILayout.ObjectField("KnightController", _fireBall.KnightController, typeof(KnightController), allowSceneObjects);
            _fireBall.FireExplosion = (GameObject)EditorGUILayout.ObjectField("FireExplosion", _fireBall.FireExplosion, typeof(GameObject), allowSceneObjects);
        }
        else if (_fireBall.FireballOwner == FireBallOwner.Dragon)
        {
            _fireBall.DragonController = (DragonController)EditorGUILayout.ObjectField("DragonController", _fireBall.DragonController, typeof(DragonController), allowSceneObjects);
            _fireBall.FireExplosion = (GameObject)EditorGUILayout.ObjectField("FireExplosion", _fireBall.FireExplosion, typeof(GameObject), allowSceneObjects);
        }
    }
}
