﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour {

    [SerializeField]
    private Slider _healthBar;
    [SerializeField]
    private Slider _manaBar;
    [SerializeField]
    private Text _healthText;
    [SerializeField]
    private Text _manaText;
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private InventoryItem _inventoryItemPrefab;
    [SerializeField]
    private Transform _inventoryContainer;
    [SerializeField]
    private GameObject[] _window;
    [SerializeField]
    private Text[] _infoBar;
    [SerializeField]
    private Button _button;

    private static HUD _instance;
    private bool _openWindow = false;

    public static HUD Instance
    {
        get
        {
            return _instance;
        }
    }
    public Slider HealthBar
    {
        get
        {
            return _healthBar;
        }

        set
        {
            _healthBar = value;
        }
    }
    public Text HealthText
    {
        get
        {
            return _healthText;
        }

        set
        {
            _healthText = value;
        }
    }
    public Text ScoreText
    {
        get
        {
            return _scoreText;
        }

        set
        {
            _scoreText = value;
        }
    }
    public Text[] InfoBar
    {
        get
        {
            return _infoBar;
        }

        set
        {
            _infoBar = value;
        }
    }
    public Text ManaText
    {
        get
        {
            return _manaText;
        }

        set
        {
            _manaText = value;
        }
    }
    public Slider ManaBar
    {
        get
        {
            return _manaBar;
        }

        set
        {
            _manaBar = value;
        }
    }

    private void Awake()
    {
        _instance = this;
    }
    void Start () {
		
	}

	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Cancel"))
        {
            if (!_openWindow)
            {
                _window[1].SetActive(true);
                _button.Select();
                _openWindow = true;
                GameManager.Instance.State = GameState.Pause;
            }
            else
            {
                _window[1].SetActive(false);
                _openWindow = false;
                GameManager.Instance.State = GameState.Play;
            }
        }
    }
    public void ShowAndHideWindow(GameObject window)
    {
        if (!_openWindow)
        {
            _openWindow = true;
            InventoryWindow();
            GameManager.Instance.State = GameState.Pause;
        }
        else
        {
            _openWindow = false;
            var _animatorStateInfo = window.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
            float _animationLength = _animatorStateInfo.length;
            window.GetComponent<Animator>().SetBool("Close", true);
            Invoke("InventoryWindow", _animationLength);
            GameManager.Instance.State = GameState.Play;

        }
    }
    void InventoryWindow()
    {
        if(_openWindow)
            _window[0].SetActive(true);
        else
            _window[0].SetActive(false);
    }
    public InventoryItem AddNewInventoryItem(Treasure treasure, int amount)
    {
        InventoryItem newitem = Instantiate(_inventoryItemPrefab) as InventoryItem;
        newitem.transform.SetParent(_inventoryContainer);
        newitem.transform.localScale = Vector3.one;
        newitem.Image.sprite = newitem.ImageContainer[(int)treasure - 1];
        newitem.Quantity = amount;
        newitem.Treasure = treasure;
        
        return newitem;
    }
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
    }
}
