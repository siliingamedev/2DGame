﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightWalk : MonoBehaviour {
    [SerializeField]
    private KnightController knightController;

    private bool _nextAttack = true;

    public bool NextAttack
    {
        get
        {
            return _nextAttack;
        }

    }

    public void WalkActive()
    {
        GameManager.Instance.AudioManager.SfxVolume = 0.3f;
        if(knightController.GroundType == GroundType.Ground)
            GameManager.Instance.AudioManager.PlaySoundRandomPitch("step");
        else if (knightController.GroundType == GroundType.Wood)
            GameManager.Instance.AudioManager.PlaySoundRandomPitch("step_wood");
    }
    public void StartWalk()
    {
        knightController.CanWalk = true;
    }
    public void NextAttackTrue()
    {
        GetComponent<Animator>().SetBool("next_Attack", true);
        _nextAttack = true;
    }
    public void NextAttackFalse()
    {
        GetComponent<Animator>().SetBool("next_Attack", false);
        _nextAttack = false;
    }
}
