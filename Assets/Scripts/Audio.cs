﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class Audio {

    #region Private_Variables
    private AudioSource _sourceSFX;
    private AudioSource _sourceMusic;
    private AudioSource _sourceRandomPitchSFX;

    private float _musicVolume = 1f;
    private float _sfxVolume = 1f;

    [SerializeField]
    private AudioClip[] _sounds;
    [SerializeField]
    private AudioClip _defaultSound;
    [SerializeField]
    private AudioClip _menuMusic;
    [SerializeField]
    private AudioClip _gameMusic;
    #endregion
    #region Properties
    public float MusicVolume
    {
        get
        {
            return _musicVolume;
        }

        set
        {
            _musicVolume = value;
        }
    }

    public float SfxVolume
    {
        get
        {
            return _sfxVolume;
        }

        set
        {
            _sfxVolume = value;
        }
    }

    public AudioSource SourceSFX
    {
        get
        {
            return _sourceSFX;
        }

        set
        {
            _sourceSFX = value;
        }
    }

    public AudioSource SourceMusic
    {
        get
        {
            return _sourceMusic;
        }

        set
        {
            _sourceMusic = value;
        }
    }

    public AudioSource SourceRandomPitchSFX
    {
        get
        {
            return _sourceRandomPitchSFX;
        }

        set
        {
            _sourceRandomPitchSFX = value;
        }
    }
    #endregion
    private AudioClip GetSound(string clipName)
    {
        for (int i = 0; i < _sounds.Length; i++)
        {
            if (_sounds[i].name == clipName)
            {
                return _sounds[i];
            }
        }
        Debug.LogError("Can not find clip " + clipName);
        return _defaultSound;
    }
    public void PlaySound(string clipName)
    {
        SourceSFX.PlayOneShot(GetSound(clipName), _sfxVolume);  
    }
    public void PlaySoundRandomPitch(string clipName)
    {
        SourceRandomPitchSFX.pitch = Random.Range(0.7f, 1.3f);
        SourceRandomPitchSFX.PlayOneShot(GetSound(clipName), _sfxVolume);
    }
    public void StopSound()
    {
        SourceSFX.Pause();
    }
    public void PlayMusic(bool menu)
    {
        if (menu)
        {
            SourceMusic.clip = _menuMusic;
        }
        else SourceMusic.clip = _gameMusic;
        SourceMusic.volume = _musicVolume;
        SourceMusic.loop = true;
        SourceMusic.Play();
    }
}
