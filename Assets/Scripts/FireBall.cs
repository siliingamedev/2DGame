﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FireBallOwner {Dragon,Knight}
public class FireBall : MonoBehaviour {

    [SerializeField]
    private GameObject _fireExplosion;
    [SerializeField]
    private DragonController _dragonController;
    [SerializeField]
    private KnightController _knightController;
    [SerializeField]
    private FireBallOwner _fireballOwner = FireBallOwner.Knight;

    #region Properties
    public FireBallOwner FireballOwner
    {
        get
        {
            return _fireballOwner;
        }

        set
        {
            _fireballOwner = value;
        }
    }
    public GameObject FireExplosion
    {
        get
        {
            return _fireExplosion;
        }

        set
        {
            _fireExplosion = value;
        }
    }
    public DragonController DragonController
    {
        get
        {
            return _dragonController;
        }

        set
        {
            _dragonController = value;
        }
    }
    public KnightController KnightController
    {
        get
        {
            return _knightController;
        }

        set
        {
            _knightController = value;
        }
    }
    #endregion

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision != null )
        {
            if (collision.gameObject.tag != "FireBallExceptions")
            {
                Rigidbody2D rigidbody2D = GetComponent<Rigidbody2D>();
                rigidbody2D.velocity = new Vector2(0, 0);
                ChangeDirection();
                _fireExplosion.SetActive(true);
                AudioSource audioSource = GetComponent<AudioSource>();
                audioSource.mute = true;
                SpriteRenderer _spriteRenderer = GetComponent<SpriteRenderer>();
                _spriteRenderer.enabled = false;
                if (_fireballOwner == FireBallOwner.Knight)
                {
                    GameManager.Instance.AudioManager.PlaySound("explode");
                    Invoke("DestroyFireBall", 0.5f);
                }
                else if (_fireballOwner == FireBallOwner.Dragon)
                {
                    GameManager.Instance.AudioManager.PlaySound("dragon_ball_exeptions");
                    Invoke("DestroyFireBall", 1);
                }   
                IDestructible destructible = collision.GetComponent<IDestructible>();
                if (destructible != null)
                {
                    if (_fireballOwner == FireBallOwner.Knight)
                        destructible.Hit(_knightController.FireBallDamage);
                    else if(_fireballOwner == FireBallOwner.Dragon)
                        destructible.Hit(_dragonController.FireBallDamage);

                }
            }
        }
    }
    public void DestroyFireBall()
    {
        Destroy(gameObject);
    }
    public void ChangeDirection()
    {
        if (_fireballOwner == FireBallOwner.Knight)
        {
            if (_knightController.RightFireBall)
            {
                _fireExplosion.transform.localScale = new Vector3(1, 1, 1);
            }

            else if (!_knightController.RightFireBall)
            {
                _fireExplosion.transform.localScale = new Vector3(-1, 1, 1);
            }
        }
        else if (_fireballOwner == FireBallOwner.Knight)
        {
            if (_dragonController.RightFireBall)
            {
                _fireExplosion.transform.localScale = new Vector3(1, 1, 1);
            }

            else if (!_dragonController.RightFireBall)
            {
                _fireExplosion.transform.localScale = new Vector3(-1, 1, 1);
            }
        }
        
    }
}
