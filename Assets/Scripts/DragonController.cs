﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DragonAttackType {FireBallAttack, BiteAttack}
public class DragonController : Character {

    [SerializeField]
    private GameObject _knight;
    [SerializeField]
    private GameObject[] _obstacles;
    [SerializeField]
    private Transform _sight;
    [SerializeField]
    private DragonAttackType _dragonAttackType = DragonAttackType.FireBallAttack;
    [SerializeField]
    private float _fireBallDamage;


    private bool _move = true;
    private bool attackOn;
    private bool _knightOnSight;
    private bool _attack2On = false;
    private float value;


    #region Properties
    public GameObject Knight
    {
        get
        {
            return _knight;
        }

        set
        {
            _knight = value;
        }
    }
    public GameObject[] Obstacles
    {
        get
        {
            return _obstacles;
        }

        set
        {
            _obstacles = value;
        }
    }
    public DragonAttackType DragonAttackType
    {
        get
        {
            return _dragonAttackType;
        }

        set
        {
            _dragonAttackType = value;
        }
    }
    public Transform Sight
    {
        get
        {
            return _sight;
        }

        set
        {
            _sight = value;
        }
    }
    public float FireBallDamage
    {
        get
        {
            return _fireBallDamage;
        }
        set
        {
            _fireBallDamage = value;
        }
    }
    public bool KnightOnSight1
    {
        get
        {
            return _knightOnSight;
        }

        set
        {
            _knightOnSight = value;
        }
    }
    #endregion

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_dragonAttackType == DragonAttackType.BiteAttack)
        {
            
        }
        if (collision == _knight.GetComponent<Collider2D>()&& _knight != null)
        {
            attackOn = true;
            _move = false;
        }
        for (int i = 0; i < _obstacles.Length; i++)
        {
            if (collision != null && collision != _obstacles[i].GetComponent<Collider2D>())
                ChangeDirection(value, 0.75f, 0.75f);
        }
       
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == _knight.GetComponent<Collider2D>())
        {
            attackOn = false;
            _move = true;
        }
    }

    void Update()
    {
        value = transform.localScale.x*-1;
        Move(value, _move);
        if (_dragonAttackType == DragonAttackType.FireBallAttack)
        {
            if (_attack2On) base.Attack2();
            _knightOnSight = KnightOnSight();
            Debug.DrawLine(transform.position, _sight.position, Color.green);
        }
        else if (_dragonAttackType == DragonAttackType.BiteAttack)
        {
            if (attackOn) base.Attack();
        }

    }
    public override void Attack2()
    {
        if (_attack2On) FireBallAttack();
    }
    public override void Attack()
    {
        if (attackOn) CharacterAttack();
    }
    public bool KnightOnSight()
    {
        RaycastHit2D[] hits = Physics2D.LinecastAll(transform.position, _sight.position);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.gameObject.tag == _knight.gameObject.tag)
            {
                _move = false;
                _attack2On = true;
                return true;
            }
            else
            {
                _attack2On = false;
                _move = true;
            } 
        }
        return false;
    }
    public override void FireBallAttack()
    {
        FireBall = Instantiate(Resources.Load("Prefabs/DragonFireBall")) as GameObject;
        FireBall.transform.position = FireBallRespawn.position;
        Vector2 velocity = FireBall.GetComponent<Rigidbody2D>().velocity;
        if (!RightFireBall)
        {
            FireBall.transform.localScale = Vector3.one;
            velocity.x = FireBallSpeed;
        }
        else if (RightFireBall)
        {
            FireBall.transform.localScale = new Vector3(-1, 1, 1);
            velocity.x = -FireBallSpeed;
        }
        FireBall.GetComponent<Rigidbody2D>().velocity = velocity;
    }
}
