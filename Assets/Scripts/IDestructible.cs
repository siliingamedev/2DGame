﻿public interface IDestructible
{
    float Health { get; set; }
    void Hit(float damage);
    void Die();
}
