﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState {Play,Pause}

public class GameManager : MonoBehaviour {
    [SerializeField]
    private KnightController _knightController;
    [SerializeField]
    private float _dragonKillScore;
    [SerializeField]
    private float _maxHealthKnight;
    [SerializeField]
    private float _maxManaKnight;
    [SerializeField]
    private GameState _state;
    [SerializeField]
    private Audio _audioManager;

    private List<InventoryItem> inventory;
    private static GameManager _instance;
    private float score = 0;

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
    public float MaxHealthKnight
    {
        get
        {
            return _maxHealthKnight;
        }

        set
        {
            _maxHealthKnight = value;
        }
    }
    public GameState State
    {
        get
        {
            return _state;
        }

        set
        {
            if (value == GameState.Play)
            {
                Time.timeScale = 1.0f;
                _state = value;
            }
            else
            {
                Time.timeScale = 0f;
                _state = value;
            }
            
        }
    }
    public float Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }
    public float MaxManaKnight
    {
        get
        {
            return _maxManaKnight;
        }

        set
        {
            _maxManaKnight = value;
        }
    }

    public Audio AudioManager
    {
        get
        {
            return _audioManager;
        }

        set
        {
            _audioManager = value;
        }
    }

    void Awake()
    {
        _instance = this;
        _state = GameState.Play;
        inventory = new List<InventoryItem>();
        InitializedAudioManager();
    }

    private void Start()
    {
        HUD.Instance.HealthBar.maxValue = _maxHealthKnight;
        HUD.Instance.HealthBar.value = _maxHealthKnight;
        HUD.Instance.HealthText.text = _maxHealthKnight +"/"+_maxHealthKnight;
        HUD.Instance.ScoreText.text = score.ToString();
        HUD.Instance.InfoBar[0].text = "HEALTH: " + _maxHealthKnight.ToString();
        HUD.Instance.ManaBar.maxValue = _maxManaKnight;
        HUD.Instance.ManaBar.value = _maxManaKnight;
        HUD.Instance.ManaText.text = _maxManaKnight + "/" + _maxManaKnight;
        HUD.Instance.InfoBar[1].text = "MANA: " + _maxManaKnight.ToString();
    }
    void Update () {
       
	}
    public void Hit(IDestructible victim)
    {
        if (victim.GetType() == typeof(DragonController))
        {
            if (victim.Health <= 0)
            {
                score += _dragonKillScore;
                HUD.Instance.ScoreText.text = score.ToString();
                Debug.Log("+50");
            }

        }
        if (victim.GetType() == typeof(KnightController))
        {
            HUD.Instance.HealthBar.value = victim.Health;
            HUD.Instance.HealthText.text = victim.Health + "/" + _maxHealthKnight;
        }
    }
    public void AddNewInventoryItem(Treasure treasure,int amount)
    {
        inventory.Add(HUD.Instance.AddNewInventoryItem(treasure,amount));
    }
    public void InventoryItemUsed(InventoryItem item)
    {
        switch (item.Treasure.ToString())
        {
            case "Health":
                _knightController.Health += item.Quantity;
                if (_knightController.Health > _maxHealthKnight)
                {
                    _knightController.Health = _maxHealthKnight;
                }
                HUD.Instance.HealthText.text = _knightController.Health + "/" + _maxHealthKnight;
                HUD.Instance.HealthBar.value = _knightController.Health;
                break;
            case "Mana":
                _knightController.Mana += item.Quantity;
                if (_knightController.Mana > _maxManaKnight)
                {
                    _knightController.Mana = _maxManaKnight;
                }
                HUD.Instance.ManaText.text = _knightController.Mana + "/" + _maxManaKnight;
                HUD.Instance.ManaBar.value = _knightController.Mana;
                break;
            case "Armor":
                
                break;
            case "Sword":
               
                break;
            case "ScrollHealth":
                _maxHealthKnight += item.Quantity;
                HUD.Instance.HealthText.text = _knightController.Health + "/" + _maxHealthKnight;
                HUD.Instance.HealthBar.maxValue = _maxHealthKnight;
                HUD.Instance.InfoBar[0].text = "HEALTH: " + _maxHealthKnight.ToString();
                break;
            case "ScrollMana":
                _maxManaKnight += item.Quantity;
                HUD.Instance.ManaText.text = _knightController.Mana + "/" + _maxManaKnight;
                HUD.Instance.ManaBar.maxValue = _maxManaKnight;
                HUD.Instance.InfoBar[1].text = "MANA: " + _maxManaKnight.ToString();
                break;
            default:
                Debug.Log("Eror!!!!Wrong Treasure name");
                break;
        }
    }
    private void InitializedAudioManager()
    {
        AudioManager.SourceSFX = gameObject.AddComponent<AudioSource>();
        AudioManager.SourceRandomPitchSFX = gameObject.AddComponent<AudioSource>();
        AudioManager.SourceMusic = gameObject.AddComponent<AudioSource>();
        gameObject.AddComponent<AudioListener>();
    }
}
