﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Treasure {Random, Health, Mana, Money, Armor, Sword, ScrollHealth, ScrollMana}
public class Chest : MonoBehaviour {

    [SerializeField]
    private Treasure _treasure;
    [SerializeField]
    private int _amount;
    private bool _chestOpened = false;
    private bool _onTrigger = false;

    public int Amount
    {
        get
        {
            return _amount;
        }
    }

    public Treasure _Treasure
    {
        get
        {
            return _treasure;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_treasure == Treasure.Random)
        {
            _treasure = (Treasure)Random.Range(1, 8);
        }
        if (_amount == 0)
        {
            _amount = Random.Range(1, 11);
        }
        if(!_chestOpened)
        {
            GetComponentInChildren<ChestContent>().ChangeImage((int)_treasure);
            GetComponentInChildren<ChestContent>().Treasure = _treasure;
            GetComponentInChildren<ChestContent>().Amount = _amount;
        }
        KnightController _knightController = collision.gameObject.GetComponent<KnightController>();

        if (_knightController != null)
        {
            _onTrigger = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        KnightController _knightController = collision.gameObject.GetComponent<KnightController>();

        if (_knightController != null)
        {
            _onTrigger = false;
        }
    }


    // Update is called once per frame
    void Update () {
        if (_onTrigger && !_chestOpened && Input.GetButton("Interaction"))
        {
            _chestOpened = true;
            GetComponent<Animator>().SetTrigger("Open");
            GameManager.Instance.AudioManager.PlaySound("chest_open");

        }
    }
}
