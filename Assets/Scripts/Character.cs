﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour, IDestructible, IAttack, IMove {

    [SerializeField]
    private float speed;
    [SerializeField]
    private float _health;
    [SerializeField]
    private float damage;
    [SerializeField]
    private float _fireBallSpeed;
    [SerializeField]
    private float attackRadius = 0.5f;
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private Rigidbody2D rigidBody2D;
    [SerializeField]
    private Transform attackPoint;
    [SerializeField]
    private Transform _fireBallRespawn;


    private GameObject _fireBall;
    private bool _rightFireBall = true;

    #region Properties
    public Animator Animator
    {
        get { return _animator; }
        set { _animator = value; }
    }
    public Rigidbody2D RigidBody2D
    {
        get
        {
            return rigidBody2D;
        }
        set
        {
            rigidBody2D = value;
        }
    }
    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }
    public float Damage
    {
        get
        {
            return damage;
        }

        set
        {
            damage = value;
        }
    }
    public float Health
    {
        get { return _health; }
        set { _health = value; }
    }
    public GameObject FireBall
    {
        get
        {
            return _fireBall;
        }

        set
        {
            _fireBall = value;
        }
    }
    public bool RightFireBall
    {
        get
        {
            return _rightFireBall;
        }
    }
    public Transform AttackPoint
    {
        get
        {
            return attackPoint;
        }

        set
        {
            attackPoint = value;
        }
    }
    public Transform FireBallRespawn
    {
        get
        {
            return _fireBallRespawn;
        }

        set
        {
            _fireBallRespawn = value;
        }
    }
    public float AttackRadius
    {
        get
        {
            return attackRadius;
        }

        set
        {
            attackRadius = value;
        }
    }
    public float FireBallSpeed
    {
        get
        {
            return _fireBallSpeed;
        }

        set
        {
            _fireBallSpeed = value;
        }
    }
   
    #endregion

    //public virtual void Move(float value)
    //{
    //    Vector2 velocity = rigidBody2D.velocity;
    //    velocity.x = value * speed;
    //    rigidBody2D.velocity = velocity;
    //}
    public virtual void Move(float value, bool move)
    {
        if (move)
        {
            Vector2 velocity = rigidBody2D.velocity;
            velocity.x = value * speed;
            rigidBody2D.velocity = velocity;
        }
        else StopMove();
    }
    public void StopMove()
    {
        Vector2 velocity = rigidBody2D.velocity;
        velocity.x = 0;
        rigidBody2D.velocity = velocity;
    }
    public virtual void Attack()
    {
        _animator.SetTrigger("attack");
    }
    public virtual void Attack2()
    {
        _animator.SetTrigger("attack2");
       
    }
    public void ChangeDirection(float value,float x, float y)
    {
        if (transform.localScale.x < 0)
        {
            if (value > 0)
            {
                transform.localScale = new Vector3(x, y, 1);
                _rightFireBall = true;
            }
        }
        else
        {
            if (value < 0)
            {
                transform.localScale = new Vector3(-x, y, 1);
                _rightFireBall = false;
            }
        }
    }
    public virtual void FireBallAttack()
    {
        _fireBall = Instantiate(Resources.Load("Prefabs/FireBallKnight")) as GameObject;
        _fireBall.transform.position = _fireBallRespawn.position; 
        Vector2 velocity = _fireBall.GetComponent<Rigidbody2D>().velocity;
        if (_rightFireBall)
        {
            _fireBall.transform.localScale = Vector3.one;
            velocity.x = _fireBallSpeed;
        }
        else if (!_rightFireBall)
        {
            _fireBall.transform.localScale = new Vector3(-1, 1, 1);
            velocity.x = -_fireBallSpeed;
        }
        _fireBall.GetComponent<Rigidbody2D>().velocity = velocity;
    }
    public void CharacterAttack()
    {
        Collider2D[] hits = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius);
        for (int i = 0; i < hits.Length; i++)
        {
            if (!GameObject.Equals(hits[i].gameObject, gameObject))
            {
                IDestructible destructible = hits[i].gameObject.GetComponent<IDestructible>();
                if (destructible != null)
                {
                    destructible.Hit(damage);
                    break;
                }
            }
        }
    }
    public virtual void Die()
    {
        Destroy(gameObject);
    }
    public void Hit(float damage)
    {
        _health -= damage;
        GameManager.Instance.Hit(this);
        if (_health <= 0)
            Die();
    }

}