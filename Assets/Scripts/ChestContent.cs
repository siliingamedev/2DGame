﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestContent : MonoBehaviour {

    [SerializeField]
    private Sprite[] _imageContainer;
    private int _amount;
    private Treasure _treasure;

    public Treasure Treasure
    {
        get
        {
            return _treasure;
        }

        set
        {
            _treasure = value;
        }
    }

    public int Amount
    {
        get
        {
            return _amount;
        }

        set
        {
            _amount = value;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        KnightController knightController = collision.gameObject.GetComponent<KnightController>();

        if (knightController != null)
        {
            Distribution();
            Destroy(gameObject);
        }
    }
    public void ChangeImage(int imageNumber)
    {
        GetComponent<SpriteRenderer>().sprite = _imageContainer[imageNumber - 1];
    }
    void Distribution()
    {
        switch (_treasure.ToString())
        {
            case "Health":
                GameManager.Instance.AddNewInventoryItem(_treasure, _amount);
                break;
            case "Mana":
                GameManager.Instance.AddNewInventoryItem(_treasure, _amount);
                break;
            case "Money":
                GameManager.Instance.Score += _amount;
                HUD.Instance.ScoreText.text = GameManager.Instance.Score.ToString();
                break;
            case "Armor":
                GameManager.Instance.AddNewInventoryItem(_treasure, _amount);
                break;
            case "Sword":
                GameManager.Instance.AddNewInventoryItem(_treasure, _amount);
                break;
            case "ScrollHealth":
                GameManager.Instance.AddNewInventoryItem(_treasure, _amount);
                break;
            case "ScrollMana":
                GameManager.Instance.AddNewInventoryItem(_treasure, _amount);
                break;
            default:
                Debug.Log("Eror!!!!Wrong Treasure name");
                break;
        }
    }
}
