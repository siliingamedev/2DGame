﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exterminated : MonoBehaviour, IDestructible {

    [SerializeField]
    private float _health;

    public float Health
    {
        get
        {
            return _health;
        }

        set
        {
            _health = value;
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }
    public void Hit(float damage)
    { 
        _health = _health - damage;
        if (_health <= 0)
            Die();
    }
}
