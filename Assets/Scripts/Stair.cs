﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stair : MonoBehaviour {
    [SerializeField]
    private GameObject knight;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision == knight.GetComponent<Collider2D>())
        {
            knight.GetComponent<KnightController>().OnStair = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == knight.GetComponent<Collider2D>())
        {
            knight.GetComponent<KnightController>().OnStair = false;
        }

    }
}
