﻿interface IMove
{
    void Move(float value, bool canWalk);
    void ChangeDirection(float value,float x, float y);
}
