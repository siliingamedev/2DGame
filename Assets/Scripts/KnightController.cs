﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum GroundType { Ground, Wood, Air }

public class KnightController : Character, IJump {

    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private Transform groundCheck;
    [SerializeField]
    private Transform _jumpSmokeRespawn;
    [SerializeField]
    private KnightWalk _knightWalk;
    [SerializeField]
    private float speedOnStair;
    [SerializeField]
    private float jumpForce;
    [SerializeField]
    private float hitDelay;
    [SerializeField]
    private float _mana;
    [SerializeField]
    private float _secondAttackCost;
    [SerializeField]
    private float _fireBallDamage;
    [SerializeField]
    private bool _secondJump = true;


    private GroundType _groundType = GroundType.Ground;
    private GameObject _jumpSmoke;
    private bool onGround = true;
    private bool onStair;
    private bool _jump2;
    private bool _canWalk = true;
    private bool _fire2On = true;
    
    
    #region Properties
    public GroundType GroundType
    {
        get
        {
            return _groundType;
        }
    }
    public bool OnStair
    {
        get
        {
            return onStair;
        }

        set
        {
            if (value)
            {
                onStair = value;
            }
            else
            {
                onStair = value;

            }

        }
    }
    public float Mana
    {
        get
        {
            return _mana;
        }

        set
        {
            _mana = value;
        }
    }
    public float FireBallDamage
    {
        get
        {
            return _fireBallDamage;
        }
        set
        {
            _fireBallDamage = value;
        }
    }
    public Camera MainCamera
    {
        get
        {
            return mainCamera;
        }

        set
        {
            mainCamera = value;
        }
    }
    public Transform GroundCheck
    {
        get
        {
            return groundCheck;
        }

        set
        {
            groundCheck = value;
        }
    }
    public KnightWalk KnightWalk
    {
        get
        {
            return _knightWalk;
        }

        set
        {
            _knightWalk = value;
        }
    }
    public float SecondAttackCost
    {
        get
        {
            return _secondAttackCost;
        }

        set
        {
            _secondAttackCost = value;
        }
    }
    public bool CanWalk
    {
        get
        {
            return _canWalk;
        }

        set
        {
            _canWalk = value;
        }
    }
    public float SpeedOnStair
    {
        get
        {
            return speedOnStair;
        }

        set
        {
            speedOnStair = value;
        }
    }
    public float JumpForce
    {
        get
        {
            return jumpForce;
        }

        set
        {
            jumpForce = value;
        }
    }
    public bool SecondJump
    {
        get
        {
            return _secondJump;
        }

        set
        {
            _secondJump = value;
        }
    }
    public Transform JumpSmokeRespawn
    {
        get
        {
            return _jumpSmokeRespawn;
        }

        set
        {
            _jumpSmokeRespawn = value;
        }
    }
    #endregion

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision != null) 
        {
            if (collision.gameObject.tag != "FireBallExceptions") _fire2On = false;
            else _fire2On = true;
        }
    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        _fire2On = true;
    }
    

    void Start()
    {
        Health = GameManager.Instance.MaxHealthKnight;
        _mana = GameManager.Instance.MaxManaKnight;
    }
    void Update () {
        mainCamera.transform.position = new Vector3(transform.position.x, transform.position.y, -1);

        Move(Input.GetAxis("Horizontal"),_canWalk);
        MoveUp();
        if (Input.GetButtonDown("Protection") && _canWalk && onGround)
        {
            Protection(true);
            CanWalk = false;
        }
        else if (Input.GetButtonUp("Protection"))
        {
            Protection(false);
            CanWalk = true;
        }
        else if (Input.GetButtonDown("Fire1") && onGround)
        {
            _canWalk = false;
            Attack();
        }
        else if (Input.GetButtonDown("Fire2") && onGround && _fire2On)
        {
            _canWalk = false;
            Attack2();    
        }
        else if (Input.GetButtonDown("Jump")) Jump();
        onGround =  CheckGround();
        Animator.SetBool("jump", !onGround);
    }
    public bool CheckGround()
    {
        RaycastHit2D[] hits = Physics2D.LinecastAll(transform.position, groundCheck.position);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider == null) _groundType = GroundType.Air;
            if (hits[i].collider.gameObject.tag  == "Ground")
            {
                _groundType = GroundType.Ground;
                return true;
            }
            else if (hits[i].collider.gameObject.tag == "Wood")
            {
                _groundType = GroundType.Wood;
                return true;
            }
        }
        return false;
    }
    public void Jump()
    {
        if (onGround)
        {
            _jumpSmoke = Instantiate(Resources.Load("Prefabs/JumpSmoke")) as GameObject;
            _jumpSmoke.transform.position = _jumpSmokeRespawn.transform.position;
            RigidBody2D.AddForce(Vector2.up * jumpForce);
            GameManager.Instance.AudioManager.PlaySound("jump");
            _jump2 = true;
            

        }
        else if (_secondJump&&_jump2)
        {
            RigidBody2D.velocity = new Vector2(0,0);
            RigidBody2D.AddForce(Vector2.up * jumpForce);
            GameManager.Instance.AudioManager.PlaySound("jump");
            _jump2 = false;
        }
    }
    public override void Attack()
    {
        if (_knightWalk.NextAttack)
        {
            base.Attack();
            GameManager.Instance.AudioManager.SfxVolume = 1f;
            GameManager.Instance.AudioManager.PlaySound("sword_attack");
            Invoke("CharacterAttack", hitDelay);
        }
    }
    public override void Attack2()
    {
        if (_knightWalk.NextAttack)
        {
            if (_mana > 0 || _mana >= _secondAttackCost)
            {
                base.Attack2();
                Invoke("FireBallAttack", hitDelay);
                _mana -= _secondAttackCost;
                HUD.Instance.ManaText.text = _mana + "/" + GameManager.Instance.MaxManaKnight;
                HUD.Instance.ManaBar.value = _mana;
            }
        }
    }
    private void Protection(bool protection)
    {
        if (protection)
        {
            Animator.SetBool("protection", true);
            GameManager.Instance.AudioManager.PlaySound("shield_on");
        }
        else Animator.SetBool("protection", false);
    }
    void MoveUp()
    {
        if (onStair && Input.GetButton("Interaction"))
        {
            RigidBody2D.gravityScale = 0;
            var velocity = RigidBody2D.velocity;
            velocity.y = Input.GetAxis("Vertical") * speedOnStair;
            RigidBody2D.velocity = velocity;
        }
        else RigidBody2D.gravityScale = 1;
    }
    public override void Move(float value, bool canwalk)
    {
        base.Move(value,canwalk);
        Animator.SetFloat("speed", Mathf.Abs(value));
        ChangeDirection(value, 1, 1);
        
    }

}
