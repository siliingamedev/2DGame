﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour {

    [SerializeField]
    private Sprite[] _imageContainer;
    [SerializeField]
    private Image _image;
    [SerializeField]
    private Text _name;
    [SerializeField]
    private Text _count;

    private float _quantity;
    private Treasure _treasure;
    
    public float Quantity
    {
        get
        {
            return _quantity;
        }

        set
        {
            _quantity = value;
        }
    }
    public Treasure Treasure
    {
        get
        {
            return _treasure;
        }

        set
        {
            _treasure = value;
        }
    }
    public Sprite[] ImageContainer
    {
        get
        {
            return _imageContainer;
        }

        set
        {
            _imageContainer = value;
        }
    }
    public Image Image
    {
        get
        {
            return _image;
        }

        set
        {
            _image = value;
        }
    }

    
    void Start () {
        _name.text = _treasure.ToString().ToUpper() + "(+" + _quantity +")";
        //_count.text = HUD.Instance.InventoryCount.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Click()
    {
        GameManager.Instance.InventoryItemUsed(this);
        Destroy(gameObject);
    }
}
