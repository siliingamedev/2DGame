﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cart : MonoBehaviour {

    [SerializeField]
    private GameObject knight;

    private Rigidbody2D rigidbody2d;

    [SerializeField]
    private bool canMove;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision == knight.GetComponent<Collider2D>())
        {
            canMove = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision == knight.GetComponent<Collider2D>())
        {
            canMove = false;
        }
    }
    private void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }
    void Update () {
        if (canMove && Input.GetButton("Interaction"))
        {
            rigidbody2d.bodyType = RigidbodyType2D.Dynamic; 
        }
        else rigidbody2d.bodyType = RigidbodyType2D.Static;
    }
}
