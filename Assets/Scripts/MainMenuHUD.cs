﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuHUD : HUD {

    [SerializeField]
    private Button _startButton;

    private void Start()
    {
        _startButton.Select();
    }


}
